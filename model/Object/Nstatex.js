var query = require('../../model/dbQuerys/querys');
var qartez = require('../../model/dbQuerys/qartez');

function Nstatex(sharqId,data,nstatexGroupPrice) {

    this.sharq_id = sharqId;
    this.properties = {
        x: typeof data != "undefined" ? data.x : null,
        y: typeof data != "undefined" ? data.y : null,
        draggable: typeof data != "undefined" ? data.draggable : null,
        isRender: typeof data != "undefined" ? data.isRender  : null
    };
    this.nstatex =  typeof data != "undefined" ? data.nstatex : null;
    this.nstatexGroupPrice = nstatexGroupPrice != "undefined" ?  nstatexGroupPrice : null;;

    this.segment_id = null;
    this.result = [];


    var self = this;

    this.init = function () {
        return new Promise(function (resolve, reject) {
            self.insertNstatex()
                .then(function () {
                    resolve()
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.initSelect = function (sharq_id,segment_id,nstatexGroupPrice) {
        this.sharq_id = sharq_id;
        this.segment_id = segment_id;
        this.nstatexGroupPrice = nstatexGroupPrice;
        return new Promise(function (resolve, reject) {
            self.selectNstatex()
                .then(function () {
                    resolve(self.result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.selectNstatex = function () {
        return new Promise(function (resolve, reject) {
            qartez.findByMultyNamePromise("map_nstatex",{sharq_id:self.sharq_id},['id','sharq_id','nstatex_group_price_id','group_properties','properties'])
                .then(function (data) {
                    // console.log("data",data)

                    let mNstatexGroup = []
                    for(let i=0;i<data.length;i++){
                        let j = 0;
                        let foundGroupId = false;
                        let mgroup_properties = JSON.parse(data[i]['group_properties']);
                        let properties = JSON.parse(data[i]['properties']);
                        mgroup_properties['id'] = "group_tex_"+data[i]['id'];
                        mgroup_properties['name'] = "group_tex_"+data[i]['id'];
                        do {
                            if(mNstatexGroup.length == 0){
                                foundGroupId = false;
                                break;
                            }else{
                                if(mNstatexGroup[j]['checkId'] == properties['id']){
                                    foundGroupId = true;
                                    break;
                                }
                            }
                            j++;
                        } while (mNstatexGroup.length > j);

                        if(!foundGroupId){
                            mNstatexGroup.push({
                                checkId:properties['id'],
                                id: mgroup_properties['id'],
                                name: mgroup_properties['name'],
                                x: mgroup_properties['x'],
                                y: mgroup_properties['y'],
                                draggable: mgroup_properties['draggable'],
                                isRender: mgroup_properties['isRender'],
                                parentinfo: {segment: self.segment_id, sharq: data[i]['sharq_id']},
                                nstatex: []
                            })
                        }
                    }

                    for(let i=0;i<mNstatexGroup.length;i++){
                        for(let j=0;j<data.length;j++){
                            let properties = JSON.parse(data[j]['properties']);
                            if(mNstatexGroup[i]['checkId'] == properties['id']){

                                for(let k=0;k<self.nstatexGroupPrice.length;k++){
                                    if(self.nstatexGroupPrice[k]['id'] == data[j]['nstatex_group_price_id']){
                                        properties['group'] = self.nstatexGroupPrice[k]
                                    }
                                }
                                properties['id'] = data[j]['id'];
                                mNstatexGroup[i]['nstatex'].push(properties)
                            }
                        }
                    }
                    console.log("mNstatexGroup",mNstatexGroup)
                    self.result = mNstatexGroup;

                    // let group_properties = JSON.parse(data[0]['group_properties']);
                    // group_properties['id'] = "group_tex_"+data[0]['id'];
                    // group_properties['name'] = "group_tex_"+data[0]['id'];
                    // let nstatexGroup = {
                    //     id: group_properties['id'],
                    //     name: group_properties['name'],
                    //     x: group_properties['x'],
                    //     y: group_properties['y'],
                    //     draggable: group_properties['draggable'],
                    //     isRender: group_properties['isRender'],
                    //     parentinfo: {segment: self.segment_id, sharq: data[0]['sharq_id']},
                    //     nstatex: []
                    // }
                    // for(let i=0;i<data.length;i++){
                    //     let children = JSON.parse(data[i]['properties']);
                    //     children['id'] = data[i]['id'];
                    //     children['parentinfo'] = {segment: self.segment_id, sharq: data[0]['sharq_id']}
                    //
                    //     for(let k=0;k<self.nstatexGroupPrice.length;k++){
                    //         if(self.nstatexGroupPrice[k]['id'] == data[0]['nstatex_group_price_id']){
                    //             children['group'] = self.nstatexGroupPrice[k]
                    //         }
                    //     }
                    //
                    //     nstatexGroup.nstatex.push(children)
                    // }
                    //
                    // self.result = nstatexGroup;
                    resolve()
                })
                .catch(function (error) {
                    console.log("//////////////",error)
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.insertNstatex = function () {
        return new Promise(function (resolve, reject) {

            let queryNstatex = [];
            for(let i=0;i<self.nstatex.length;i++){
                let insertProperties = {
                    number: self.nstatex[i]['number'],
                    id:self.nstatex[i]['id'],
                    x: self.nstatex[i]['x'],
                    y: self.nstatex[i]['y'],
                    width: self.nstatex[i]['width'],
                    height: self.nstatex[i]['height'],
                    draggable: self.nstatex[i]['draggable'],
                    fill: self.nstatex[i]['fill'],
                    stroke: self.nstatex[i]['stroke'],
                    strokeWidth: self.nstatex[i]['strokeWidth'],
                    cornerRadius: self.nstatex[i]['cornerRadius'],
                    shadowBlur: self.nstatex[i]['shadowBlur'],
                    shadowColor: self.nstatex[i]['shadowColor'],
                    isSelect: self.nstatex[i]['isSelect']
                }

                let nstatex_group_price_id = null;
                for(let j=0;j<self.nstatexGroupPrice.length;j++){
                    if(self.nstatexGroupPrice[j]['id'] == self.nstatex[i]['group']['id']){
                        nstatex_group_price_id = self.nstatexGroupPrice[j]['server_id'];
                        break;
                    }
                }

                queryNstatex.push(
                    query.insert2vPromise("map_nstatex",{
                        sharq_id: self.sharq_id,
                        nstatex_group_price_id:nstatex_group_price_id,
                        group_properties:JSON.stringify(self.properties),
                        properties:JSON.stringify(insertProperties)
                    })
                )
                console.log("i",i)
            }

            Promise.all(queryNstatex)
                .then(function (result) {
                    resolve()
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }
}


module.exports = Nstatex;
