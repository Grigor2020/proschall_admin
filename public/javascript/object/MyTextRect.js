class MyTextRect {

    constructor(id,title,x,y,w,h,fill,stroke,stokeW,draggable,parentsInfo){
        this.id = id;
        this.title = title;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.fill = null,//fill || null;
        this.stroke = stroke || null;
        this.strokeWidth = stokeW || null;
        this.draggable = draggable || false;

        this.parentinfo = {
            segment:parentsInfo.segment
        };

        this.isRender = false;
        this.parent = null;
        this.rect = null;
        this.nameLable = null;
    }

    init(){
        // console.log("parent",parents)
        // this.parent = parents;
        this.nameLable = new Konva.Text({
            id:"id"+this.id,
            name:"name"+this.id,
            x: this.x,
            y: this.y,
            fontSize: 14,
            text: this.title,
            fill:"#ffffff",
            isTransform:false,
            draggable: this.draggable,
            // parentinfo: this.parentinfo
        });

        this.rect = new Konva.Transformer({
            id:"id"+this.id,
            name:"name"+this.id,
            typeobj:"text",
            node: this.nameLable,
            isTransform:false,
            centeredScaling: false,
            rotationSnaps: [0, 90, 180, 270],
            resizeEnabled: false
        });
    }

    initEvents(){
        var self = this;
        this.rect.on('transformstart', function() {
            // console.log('transform start');
        });

        this.rect.on('dragmove', function() {
            //this.layer.batchDraw();
        });
        this.rect.on('transform', function() {
            getSelectObjInfo(self.rect)
        });

        this.rect.on('transformend', function() {
            getSelectObjInfo(self.rect)
        });

        this.rect.on('mousedown', function() {
            removeNstatexSelect();
            removeSelectedNstatexSelect();
        });

        this.nameLable.on('mousedown', function() {
           self.onMouseDown()
        });

        this.parent.on('dragmove', function() {
            getSelectObjInfo(self.rect)
        });

        this.rect.on('dblclick', function() {
            self.getCoordinates()
        });
    }

    setParent(parents){
        this.parent = parents;
        this.initEvents();
    }

    onMouseDown() {
        removeNstatexSelect();
        removeSelectedNstatexSelect();
        getSelectObjInfo(this.rect);
        triggerMouseEvent (this.rect, "mousedown");
    }

    updateText(){
        let self = this;
        this.nameLable['children'][0].setAttrs({
            text: self.rect.title()
        });

        this.rect.setAttrs({
            width: this.nameLable.width(),
            height: this.nameLable.height()
        })
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.rect.x(),
            'y: ' + this.rect.y(),
            'rotation: ' + this.rect.rotation(),
            'width: ' + this.rect.width(),
            'height: ' + this.rect.height(),
            'scaleX: ' + this.rect.scaleX(),
            'scaleY: ' + this.rect.scaleY()
        ];
        console.log(lines.join('\n'));
    }

    getObject(){
        return this.rect;
    }

    getLableObject(){
        return this.nameLable;
    }
}
