var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var staticMethods = require('../../model/staticMethods');

router.get('/', function(req, res, next) {
    query.findAllTransactionHistory()
        .then(function (pageInfo){
            res.render('Profile/transactions',
                {
                    userInfo:req.userInfo,
                    language : req.language,
                    siteData:req.siteData,
                    // selfPermissions : checkPermission.readOrWrite,
                    data:pageInfo
                });
        })
        .catch(function (){
            res.json({error:true})
        })


    // })
});
router.get('/detail/:id', function(req, res, next) {
    var transactionId = req.params.id;
    query.findTransactionHistory(transactionId)
        .then(function (pageInfo){
            res.render('Profile/transactions',
                {
                    userInfo:req.userInfo,
                    language : req.language,
                    siteData:req.siteData,
                    // selfPermissions : checkPermission.readOrWrite,
                    data:pageInfo
                });

        })
        .catch(function (){
            res.json({error:true})
        })

    // })
});

module.exports = router;
