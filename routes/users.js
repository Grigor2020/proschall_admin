var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');
var staticMethods = require('../model/staticMethods');
var sha1 = require('sha1');


router.get('/', function(req, res, next) {
    query.findAllPromise('users_db',null)
        .then(function (data) {
            res.render('users/users', {
                users:data,
                siteData:req.siteData,
                userInfo : req.userInfo
            });
        })
        .catch(function (error) {
            console.log({error:true,msg:'84532186315',errMsg: error.toString()});
            res.end();
        })
});
module.exports = router;
