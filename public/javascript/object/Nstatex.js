class Nstatex {

    constructor(number,id,x,y,w,h,imageObj,draggable,parentsInfo){
        this.number = number;
        this.id = id;
        this.x = x;
        this.y = y;
        this.image = imageObj;
        this.width = w;
        this.height = h;
        this.draggable = draggable || false;

        this.parentinfo = {
            segment:parentsInfo.segment,
            sharq:parentsInfo.sharq
        };

        this.imageTooltip = null;
        this.imageKonva = null;
    }

    init(){
        this.imageKonva = new Konva.Image({
            typeobj:"nstatex",
            number:this.number,
            id:"id_"+this.id,
            name:"nstatex_"+this.id,
            isTransform:false,
            image: this.image,
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            draggable: this.draggable,
            parentinfo:this.parentinfo,
            dragBoundFunc: function(pos) {
                return {
                    x: pos.x,
                    y: this.absolutePosition().y
                };
            }
        });

        this.generateToltip();
    }

    generateToltip(){
        var self = this;

        this.imageTooltip = new Konva.Label({
            x: self.imageKonva.attrs.x + self.width/2,
            y: self.imageKonva.attrs.y - 5,
            opacity: 0.75,
            visible: false
        });

        this.imageTooltip.add(
            new Konva.Tag({
                fill: 'black',
                pointerDirection: 'down',
                pointerWidth: 10,
                pointerHeight: 10,
                lineJoin: 'round',
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffsetX: 10,
                shadowOffsetY: 10,
                shadowOpacity: 0.5
            })
        );

        let textShow = this.parentinfo.segment+"\n Շարք: "+this.parentinfo.sharq+"\n Տեղ: "+this.number;
        this.imageTooltip.add(
            new Konva.Text({
                text: textShow,
                fontFamily: 'Calibri',
                fontSize: 18,
                padding: 5,
                fill: 'white'
            })
        );
    }

    updateText(){
        let textShow = this.parentinfo.segment+"\n Շարք: "+this.parentinfo.sharq+"\n Տեղ: "+this.number;
        this.imageTooltip['children'][1].setAttrs({
            text: textShow
        });
    }

    initEvents(){
        var self = this;
        // this.rect.on('transformstart', function() {
        //     // console.log('transform start');
        // });
        //
        // this.rect.on('dragmove', function() {
        //     //this.layer.batchDraw();
        // });
        // this.rect.on('transform', function() {
        //     this.setAttrs({
        //         width: this.width() * this.scaleX(),
        //         height: this.height() * this.scaleY(),
        //         scaleX: 1,
        //         scaleY: 1
        //     });
        //     // console.log('transform');
        // });
        //
        // this.rect.on('transformend', function() {
        //     // console.log('transform end');
        // });

        this.imageKonva.on('mousemove', function() {
            self.imageTooltip.show();
        });

        this.imageKonva.on('mouseout', function() {
            self.imageTooltip.hide();
        });

        this.imageKonva.on('mousedown', function() {
            getSelectObjInfo(self.imageKonva)
        });

        this.imageKonva.on('dblclick', function() {
            self.getCoordinates()
        });

        this.parent.on('dragmove', function() {
            getSelectObjInfo(self.imageKonva)
        });
    }

    setParent(parents){
        this.parent = parents;
        this.initEvents();
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.imageKonva.x(),
            'y: ' + this.imageKonva.y(),
            'rotation: ' + this.imageKonva.rotation(),
            'width: ' + this.imageKonva.width(),
            'height: ' + this.imageKonva.height(),
            'scaleX: ' + this.imageKonva.scaleX(),
            'scaleY: ' + this.imageKonva.scaleY()
        ];
        // console.log(lines.join('\n'));
    }

    getObject(){
        return this.imageKonva;
    }

    getTootipObject(){
        return this.imageTooltip;
    }

}
