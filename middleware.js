var statics = require('./static');
var query = require('./model/dbQuerys/querys')

const getOurMoney = function (req, res, next) {
    query.findAllOurMoney()
        .then(function (data){
            // console.log('data',data)
            req.siteData.ourMoney = data[0].summary_money;
            // console.log(' req.siteData.ourMoney', req.siteData.ourMoney)
            next();
        })
        .catch(function (error){
            req.siteData.ourMoney = 0
            next();
        })

}

module.exports.getOurMoney = getOurMoney;
