const db = require('../connection');
var log = require("../../logs/log");

var addNotification = function(params){
    console.log('params',params)
    return new Promise(function (resolve, reject) {
        // var insertParams = {
        //     user_id : typeof params.creatorId !== "undefined" ? params.creatorId : null,
        //     s_user_id :typeof params.joinerId !== "undefined" ? params.joinerId : null,
        //     type : params.type,
        //     view : "0",
        // }
        var prepareSql = 'INSERT INTO notifications SET ?';
        var query = db.query(prepareSql, params, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"addNotification",
                        table:"notifications",
                        params:params,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}


var findUserNotifications = function(user_id){
    // console.log('asdasd',user_id)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `notifications`.`user_id`,`notifications`.`id` as notificationId, ' +
            '`notifications`.`s_user_id`,`notifications`.`view`,`notifications`.`type`,`notifications`.`challenge_id`, ' +
            '`users_db`.`username` as s_user_username,`matches`.`id` as matchesId  ' +
            'FROM `notifications` ' +
            'LEFT JOIN `users_db` ON `users_db`.`id` = `notifications`.`s_user_id` ' +
            'LEFT JOIN `matches` ON ' +
            ' `matches`.`challenge_id` = `notifications`.`challenge_id` AND  ' +
            ' `matches`.`creator_id` = `notifications`.`user_id` AND ' +
            ' `matches`.`joiner_id` = `notifications`.`s_user_id` ' +
            'WHERE `notifications`.`user_id` = '+db.escape(user_id)+' ORDER BY  `notifications`.`id` DESC';
        // var prepareSql = '' +
        //     'SELECT `notifications`.`user_id`,`notifications`.`id` as notificationId, ' +
        //     '`notifications`.`s_user_id`,`notifications`.`view`,`notifications`.`type`,`notifications`.`challenge_id`, ' +
        //     '`users_db`.`username` as s_user_username ' +
        //     'FROM `notifications` ' +
        //     'JOIN `users_db` ON `users_db`.`id` = `notifications`.`s_user_id` ' +
        //     'WHERE `notifications`.`user_id` = '+db.escape(user_id)+' ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserNotofications",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error findByUrl: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findUserUnreadCount = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT  COUNT(`notifications`.`id`) as counts ' +
            'FROM `notifications` ' +
            'WHERE `notifications`.`user_id` = '+db.escape(user_id)+' AND `notifications`.`view` = "0"';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserNotofications",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error findByUrl: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


var setViewedNotification = function(notificationId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'UPDATE `notifications` ' +
            'SET view="1" ' +
            'WHERE id='+db.escape(notificationId)+' ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"setViewedNotification",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/notification.txt")
                reject(new Error("Sql error setViewedNotification: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


module.exports.addNotification = addNotification;
module.exports.findUserNotifications = findUserNotifications;
module.exports.setViewedNotification = setViewedNotification;
module.exports.findUserUnreadCount = findUserUnreadCount;
