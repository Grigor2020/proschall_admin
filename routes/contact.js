var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');


router.get('/', function(req, res, next) {
    res.render('contact', {
        userInfo:req.userInfo,
        siteData:req.siteData,
        checkSuccessMsg:0
    });
});

router.post('/', function(req, res, next) {
    var params = {
        user_id: req.userInfo['id'],
        phone: req.body.phone,
        email: req.body.email,
        text: req.body.text
    };
    query.insert2v('ac_contact_us',params, function (error,data) {
        var checkSuccessMsg = null;
        if(error){
            checkSuccessMsg = 1
        }else{
            checkSuccessMsg = 2
        }
        res.render('pu_contact_us',
            {
                userInfo:req.userInfo,
                checkSuccessMsg:checkSuccessMsg
            });
    })
});

module.exports = router;
