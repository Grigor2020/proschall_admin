class MyCircle {

    constructor(id,x,y,r,fill,stroke,stokeW,parent){
        this.id = id;
        this.x = x;
        this.y = y;
        this.radius = r;
        this.fill = fill || null;
        this.stroke = stroke || null;
        this.strokeWidth = stokeW || null;

        this.isRender = false;
        this.circle  = null;
        this.parent = parent;
    }

    init(){
        this.circle  = new Konva.Circle({
            id:this.id,
            name:"klor_"+this.id,
            x: this.x,
            y: this.y,
            radius: this.radius,
            fill: this.fill,
            stroke: this.stroke,
            strokeWidth: this.strokeWidth,
            draggable: false
        })

        this.initEvents();
    }

    initEvents(){
        var self = this;

        this.circle.on('mousemove mousedown', function() {
            self.parent.onMouseMove();
        })

        this.circle.on('mouseout', function() {
            self.parent.onMouseOut();
        })

        this.circle.on('mousedown', function() {
            self.parent.onMouseDown();
        });

        this.circle.on('dblclick', function() {
            self.getCoordinates()
        });
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.circle.x(),
            'y: ' + this.circle.y(),
            'r: ' + this.circle.radius()
        ];
        console.log(lines.join('\n'));
    }

    getObject(){
        return this.circle;
    }



}
