var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
bodyParser.urlencoded({extended: false});
var querys = require('./model/dbQuerys/querys');
var mids = require('./middleware')
var app = express();
var url = require('url') ;

var indexRouter = require('./routes/index');
var permissionDeniedRouter = require('./routes/permission_denied');
var usersRouter = require('./routes/users');
var settingsRouter = require('./routes/profile/settings');
var transactionsRouter = require('./routes/profile/transactions');
var gamesRouter = require('./routes/games/games');
var loginRouter = require('./routes/auth/login');
var contactUsRouter = require('./routes/contact');
var depositRouter = require('./routes/deposit/deposit');
var withdrawRouter = require('./routes/withdraw/withdraw');
var disputeRouter = require('./routes/dispute/dispute');
var faqRouter = require('./routes/faq/faq');
var tournamentRouter = require('./routes/tournament/tournament');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// check Cookie exists or not
app.use(function(req, res, next) {

    var pathname = url.parse(req.url).pathname;
    var pathName = pathname.split('/');
    app.locals.pathName = pathName[1];

    var cookie = req.cookies['clin'];

    if(typeof cookie === 'undefined'){
        if(req.originalUrl === '/login'){
            next();
        }else{
            res.redirect('/login');
        }
    }else{
        querys.checkUser(cookie)
            .then(function (user) {
                if (user.length == 1) {
                    req.userInfo = user[0];
                    next();
                }else{
                    if(req.originalUrl === '/login'){
                        next();
                    }else{
                        res.redirect('/login');
                    }
                }
            })
            .catch(function (error) {
                res.redirect('/login');
            })
    }
    // next()
});

// select all require info
app.use(function(req, res, next) {
    req.siteData = {};
    req.siteData.newCounts = {};
    req.siteData.ourMoney = 0;
    var allQuery =
        [
            querys.findAllPromise('language'),
            querys.findC2CNewCount(),
            querys.findBitcoinNewCount(),
            querys.findPmNewCount()
        ];
    Promise.all(allQuery)
        .then(function ([language,C2CNewCount,BitcoinNewCount,PmNewCount]) {
            // console.log('.........',C2CNewCount[0].counts);
            req.siteData.newCounts.c2c = C2CNewCount[0].counts;
            req.siteData.newCounts.bitcoin = BitcoinNewCount[0].counts;
            req.siteData.newCounts.pm = PmNewCount[0].counts;
            req.language = language;
            next();
        })
        .catch(function (error) {
            console.log('error',error)
        })
});
app.use(function (req, res, next) {mids.getOurMoney(req, res, next)});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/contact-us', contactUsRouter);
app.use('/login', loginRouter);
app.use('/games', gamesRouter);
app.use('/profile/settings', settingsRouter);
app.use('/profile/transactions', transactionsRouter);
app.use('/deposit', depositRouter);
app.use('/withdraw', withdrawRouter);
app.use('/dispute', disputeRouter);
app.use('/permission-denied', permissionDeniedRouter);
app.use('/faq', faqRouter);
app.use('/tournament', tournamentRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.render('not_found');
    // next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
