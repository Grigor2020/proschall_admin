var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var sha1 = require('sha1');
var staticMethods = require('../../model/staticMethods');


router.get('/', function(req, res, next) {
    query.findWithdraws()
        .then(function (data) {
            res.render('Withdraw/withdraw',
                {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    language : req.language,
                    data:data
                });
            // res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});

router.get('/update/:id', function(req, res, next) {
    console.log('req.params.id',req.params.id)
    query.findOneWithdraw(req.params.id)
        .then(function (data) {
            res.render('Withdraw/withdraw_info',
                {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    language : req.language,
                    data:data[0]
                });
            // res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});

router.post('/update', function(req, res, next) {
    console.log('req.body',req.body)
    query.findByMultyNamePromise('withdraws',{id:req.body.withdraw_id},['*'])
        .then(function (withdrawData) {
            query.update2vPromise('withdraws',{where:{id:req.body.withdraw_id},values:{accept:req.body.accept}})
                .then(function () {
                    query.update2vPromise('transactions_history',{where:{user_id:withdrawData[0].user_id,withdraw_id:withdrawData[0].id},values:{way:'0'}})
                        .then(function () {
                            res.json({error:false});
                            res.end();
                        })
                        .catch(function (error) {
                            res.json({error:true});
                            res.end();
                        })
                })
                .catch(function (error) {
                    res.json({error:true});
                    res.end();
                })
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })

});

module.exports = router;
