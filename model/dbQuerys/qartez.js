const db = require('../connection');
var log = require("../../logs/log");

var selectSegmentGroup = function (mapId) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT `map`.`name`,`map`.`version`,`map_segment_group`.`properties` \n" +
            "FROM `map`\n" +
            "JOIN `map_segment_group` ON `map_segment_group`.`map_id` = `map`.`id`\n" +
            "WHERE `map`.`id` = "+db.escape(mapId)+" ";

        db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"selectSegmentGroup",
                        table:"map",
                        params:{mapId:mapId},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/map.txt")
                reject(new Error(error.sqlMessage))
            }else{
                resolve(results)
            }
        });
    })
};

var findByMultyNamePromise = function (table,params,filds) {
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for (var key in params) {
            if (params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            } else {
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE ' + whereParams.join(' AND ') + '';

        // console.log('prepareSql', prepareSql)
        db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"selectSegmentGroup",
                        table:"map",
                        params:{params:params,filds:filds},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/map.txt")
                reject(new Error(error.sqlMessage))
            }else{
                resolve(results)
            }
        });
    });
}

module.exports.selectSegmentGroup = selectSegmentGroup;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
