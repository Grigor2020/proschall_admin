class Sharq {

    constructor(id,x,y,w,h,fill,stroke,stokeW,draggable,parentsInfo,nameText,serverInfo){
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.fill = null,//fill || null;
        this.stroke = stroke || null;
        this.strokeWidth = stokeW || null;
        this.draggable = draggable || false;

        this.parentinfo = {
            segment:parentsInfo.segment,
            segment_name:parentsInfo.segment_name
        };

        this.showLeftName = true;
        this.showRightName = true;

        this.isRender = false;
        this.leftName = null;
        this.rightName = null;
        this.parent = null;
        this.rect = null;

        this.serverId = serverInfo != null ? serverInfo.id : null;
        this.showNameText = nameText || id;
        this.serverInfo = serverInfo || null;
    }

    init(){
        // console.log("parent",parents)
        // this.parent = parents;
        this.rect = new Konva.Rect({
            typeobj:"sharq",
            id:this.id,
            name:this.id,
            isTransform:true,
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            fill: this.fill,
            stroke: this.stroke,
            strokeWidth: this.strokeWidth,
            draggable: this.draggable,
            parentinfo:this.parentinfo,
            showLeftName:this.showLeftName,
            showRightName:this.showRightName,
            showNameText:this.showNameText,
            serverId:this.serverId,
            thisclass:this
        })


        if(this.serverInfo == null) {
            this.generateSharqName();
        }else{
            this.generateServerSharqName();
        }
    }

    generateSharqName(){
        var self = this;
        this.leftName = new Konva.Label({
            x: 0,
            y: self.height/3.5,
            opacity: 1,
            visible: true
        });

        this.leftName.add(
            new Konva.Text({
                text: self.showNameText,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
        this.leftName.x(this.x-this.leftName.width()-10);

        this.rightName = new Konva.Label({
            x: self.width + 10,
            y: self.height/3.5,
            opacity: 1,
            visible: true
        });

        this.rightName.add(
            new Konva.Text({
                text: self.showNameText,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
    }

    generateServerSharqName(){
        var self = this;
        this.leftName = new Konva.Label({
            x: this.serverInfo.lefLable.x,
            y: this.serverInfo.lefLable.y,
            opacity: 1,
            visible: true
        });

        this.leftName.add(
            new Konva.Text({
                text: this.serverInfo.lefText.text,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
        this.leftName.x(this.x-this.leftName.width()-10);

        this.rightName = new Konva.Label({
            x: this.serverInfo.rightLable.x,
            y: this.serverInfo.rightLable.y,
            opacity: 1,
            visible: true
        });

        this.rightName.add(
            new Konva.Text({
                text: this.serverInfo.rightText.text,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
    }

    initEvents(){
        var self = this;
        this.rect.on('transformstart', function() {
            self.saveXPoint = Math.floor(this.x());
        });

        this.rect.on('dragmove', function() {
            //this.layer.batchDraw();
        });
        this.rect.on('transform', function() {

            // self.rightName.hide();
            // self.leftName.x(self.id < 10 ? this.x() - 20 : this.x() - 30);
            self.leftName.x(this.x()-self.leftName.width()-10);
            self.rightName.x(this.width()+this.x() + 10);
            this.setAttrs({
                width: this.width() * this.scaleX(),
                height: this.height() * this.scaleY(),
                scaleX: 1,
                scaleY: 1
            });
            getSelectObjInfo(self.rect)
            // console.log('transform');
        });

        this.rect.on('transformend', function() {
            let w = Math.round(this.width()/grid)*grid;
            let h = Math.round(this.height()/grid)*grid;
            this.setAttrs({
                width: w,
                height: h
            });
            console.log(this.id())
            // self.leftName.x(this.id().toString().length < 2 ? this.x() - 20 : this.x() - 30);
            self.leftName.x(this.x()-self.leftName.width()-10);
            self.rightName.x(this.width()+this.x() + 10);
            // self.rightName.x(this.width()+this.x() + 10);
            // self.rightName.show();
            getSelectObjInfo(self.rect)
            // console.log('transform end');
        });

        this.rect.on('mousedown', function() {
            removeNstatexSelect();
            removeSelectedNstatexSelect();
        });

        this.parent.on('dragmove', function() {
            getSelectObjInfo(self.rect)
        });

        this.rect.on('dblclick', function() {
            self.getCoordinates()
        });
    }

    setParent(parents){
        this.parent = parents;
        this.initEvents();
    }

    onMouseDown() {
        removeNstatexSelect();
        removeSelectedNstatexSelect();
        getSelectObjInfo(this.rect);
        triggerMouseEvent (this.rect, "mousedown");
    }

    updateText(){
        let self = this;
        this.leftName['children'][0].setAttrs({
            text: self.rect.id()
        });
        this.rightName['children'][0].setAttrs({
            text: self.rect.id()
        });
        this.leftName.x(this.rect.x()-this.leftName.width()-10);
        this.rightName.x(this.rect.width()+this.rect.x() + 10);
    }

    showHideLeftName(){
        if(this.showLeftName){
            this.leftName.show();
        }else{
            this.leftName.hide();
        }
    }

    showHideRightName(){
        if(this.showRightName){
            this.rightName.show();
        }else{
            this.rightName.hide();
        }
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.rect.x(),
            'y: ' + this.rect.y(),
            'rotation: ' + this.rect.rotation(),
            'width: ' + this.rect.width(),
            'height: ' + this.rect.height(),
            'scaleX: ' + this.rect.scaleX(),
            'scaleY: ' + this.rect.scaleY(),
            'zIndex: ' + this.rect.zIndex()
        ];

        var linesp = [
            'x: ' + this.rect.parent.x(),
            'y: ' + this.rect.parent.y(),
            'rotation: ' + this.rect.parent.rotation(),
            'width: ' + this.rect.parent.width(),
            'height: ' + this.rect.parent.height(),
            'scaleX: ' + this.rect.parent.scaleX(),
            'scaleY: ' + this.rect.parent.scaleY(),
            'zIndex: ' + this.rect.parent.zIndex()
        ];
        console.log(lines.join('\n'));
        console.log(linesp.join('\n'));
    }

    getObject(){
        return this.rect;
    }

    getSharqName(){
        return this.leftName;
    }

    getSharqRightName(){
        return this.rightName;
    }
}
