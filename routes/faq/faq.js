var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var staticMethods = require('../../model/staticMethods');
var sha1 = require('sha1');

router.get('/', function(req, res, next) {
    query.findAllFaq()
        .then(function (data) {
            res.render('faq/faq', {
                faq:data,
                siteData:req.siteData,
                userInfo : req.userInfo
            });
        })
        .catch(function (error) {
            console.log({error:true,msg:'48+5328946156',errMsg: error.toString()});
            res.end();
        })
});

router.get('/add', function(req, res, next) {
    res.render('faq/faqItem', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language,
    });
});

router.post('/add-item', function(req, res, next) {
    console.log('....res_data',req.body.res_data);
    let selfData = JSON.parse(req.body.res_data)
    console.log('....',selfData);
});

module.exports = router;
