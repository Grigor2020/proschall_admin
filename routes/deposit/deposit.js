var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var notificationQuerys = require('../../model/dbQuerys/notificationQuerys');


router.get('/:type/update/:id', function(req, res, next) {
    let mainType = req.params.type;
    var table = "";
    if(mainType == '1'){
        // Card 2 Card
        table = 'deposit_c2c';
    }else if(mainType == '2'){
        // Bitcoin
        table = 'deposit_bitcoin';
    }else if(mainType == '3'){
        // Perfect money
        table = 'deposit_pm';
    }else{
        res.send('error')
    }
    var updateParams = {
        where:{
            deposit_id : req.params.id
        },
        values : {
            admin_check : '1'
        }
    }
    query.update2vPromise(table,updateParams)
        .then(function () {
            query.getDepositForUpdate(table,req.params.id,['*'])
                .then(function (data) {
                    console.log('.......data',data)
                    res.render('deposit/deposit_edit',{
                        userInfo:req.userInfo,
                        siteData:req.siteData,
                        data : data[0]
                    })
                })
                .catch(function (error) {
                    res.send(error)
                })
        })
        .catch(function (error) {
            res.send(error)
        })
    // query.findByMultyNamePromise(table,{deposit_id:req.params.id},['*'])

});

router.get('/:type', function(req, res, next) {
    // console.log('req.params.type',req.params.type)
    // console.log('......req.siteData.newCounts',req.siteData.newCounts)
    if(req.params.type == '1'){
        query.findC2CDeposits()
            .then(function (data) {
                res.render('deposit/deposit_c_2_c', {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    deposits:data
                });
            })
            .catch(function (error) {
                res.json({error:true, errMsg : error.toString()})
            })
    }else if(req.params.type == '2'){
        query.findBitcoinDeposits()
            .then(function (data) {
                res.render('deposit/deposit_bitcoin', {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    deposits:data
                });
            })
            .catch(function (error) {
                res.json({error:true, errMsg : error.toString()})
            })
    }else{
        query.findPmDeposits()
            .then(function (data) {
                res.render('deposit/deposit_pm', {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    deposits:data
                });
            })
            .catch(function (error) {
                res.json({error:true, errMsg : error.toString()})
            })
    }


});

router.post('/apply',function (req,res,next) {
    console.log('req.body',req.body)
    let mainType = req.body.type;
    let table = "";
    if(mainType == '1'){
        // Card 2 Card
        table = 'deposit_c2c';
    }else if(mainType == '2'){
        // Bitcoin
        table = 'deposit_bitcoin';
    }else if(mainType == '3'){
        // Perfect money
        table = 'deposit_pm';
    }else{
        res.send('error')
    }

    if(mainType == '1' || mainType == '2'){
        query.getDepositForUpdate(table,req.body.deposit_id,['*'])
            .then(function (data) {

                let plusMoney = parseFloat(data[0].amount);
                // console.log('1. plusMoney',plusMoney)
                let userId = data[0].user_id;
                query.findByMultyNamePromise('user_money',{user_id:userId},['money'])
                    .then(function (userMoney) {

                        let user_money = parseFloat(userMoney[0].money);
                        let updateableMoney = (plusMoney + user_money).toFixed(2);
                        console.log('.......plusMoney',plusMoney)
                        console.log('.......user_money',user_money)
                        console.log('.......updateableMoney',updateableMoney)
                        // console.log('2. plusMoney',plusMoney)
                        let updateParams = {
                            where:{
                                deposit_id : req.body.deposit_id
                            },
                            values : {
                                admin_accept : '1'
                            }
                        }
                        query.update2vPromise(table,updateParams)
                            .then(function () {
                                let updateUserMoneyParams = {
                                    where:{
                                        user_id : userId
                                    },
                                    values : {
                                        money : updateableMoney
                                    }
                                }
                                query.update2vPromise('user_money',updateUserMoneyParams)
                                    .then(function () {
                                        var params = {
                                            user_id : userId,
                                            type:'13',
                                            view : "0",
                                            money : plusMoney
                                        }
                                        // console.log('params',params)
                                        notificationQuerys.addNotification(params)
                                            .then(function (data) {
                                                res.json({error:false});
                                            })
                                            .catch(function (error) {
                                                res.json({error:true, errMsg : error.toString()});
                                                res.end();
                                            })
                                    })
                                    .catch(function (error) {
                                        res.json({error:true, errMsg : error.toString()})
                                    })
                            })
                            .catch(function (error) {
                                res.json({error:true, errMsg : error.toString()})
                            })
                    })
                    .catch(function (error) {
                        res.json({error:true, errMsg : error.toString()})
                    })
            })
            .catch(function (error) {
                res.json({error:true, errMsg : error.toString()})
            })


    }else if(mainType == '3'){
        res.json({error:true, errMsg : 'no info for Perfect Money'})
    }else{
        res.json({error:true, errMsg : "XXX"})
    }


})

module.exports = router;
