var Nstatex = require('./Nstatex');
var query = require('../../model/dbQuerys/querys');
var qartez = require('../../model/dbQuerys/qartez');

function Sharq(segmentId,data,nstatexGroupPrice) {

    this.segmentId = segmentId;
    this.id = typeof data != "undefined" ? data.id : null;
    this.name = typeof data != "undefined" ? data.name : null;
    this.properties = typeof data != "undefined" ? data.properties : null;
    this.children = typeof data != "undefined" ? data.children : null;
    this.left_namelable_pro = typeof data != "undefined" ? data.left_namelable_pro : null;
    this.left_text_pro = typeof data != "undefined" ? data.left_text_pro : null;
    this.right_namelable_pro = typeof data != "undefined" ? data.right_namelable_pro : null;
    this.right_text_pro = typeof data != "undefined" ? data.right_text_pro : null;
    this.nstatexGroupPrice = nstatexGroupPrice != "undefined" ?  nstatexGroupPrice : null;
    this.nstatexGroup = typeof data != "undefined" ? data.nstatexGroup : null;

    this.sharq_id = null;

    this.result = [];

    var self = this;

    this.init = function () {
        return new Promise(function (resolve, reject) {
            self.insertSharq()
                .then(function (data) {
                    self.sharq_id = data.insertId;
                    let allNstatex = [];
                    for(let i=0;i<self.nstatexGroup.length;i++){
                        let nstatexer = new Nstatex(self.sharq_id,self.nstatexGroup[i],self.nstatexGroupPrice);
                        allNstatex.push(nstatexer.init())
                    }

                    Promise.all(allNstatex)
                        .then(function () {
                            resolve()
                        })
                        .catch(function (error) {
                            resolve({error:true,errMsg:error.toString()})
                        })

                    resolve()
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.initSelect = function (segmentId,nstatexGroupPrice) {
        this.segmentId = segmentId;
        this.nstatexGroup = nstatexGroupPrice;
        return new Promise(function (resolve, reject) {
            self.selectSharq()
                .then(function () {

                    let allNstatex = [];
                    for(let i=0;i<self.result.length;i++){
                        let nstatexer = new Nstatex(self.result[i]['serverId']);
                        allNstatex.push(nstatexer.initSelect(self.result[i]['serverId'],self.result[i]['segment_id'],nstatexGroupPrice))
                    }

                    Promise.all(allNstatex)
                        .then(function (resultNstatex) {
                            let rNstatex = resultNstatex;
                            // console.log("///////////////////",rNstatex[0])
                            for(let i=0;i<rNstatex.length;i++){
                                for(let c=0;c<rNstatex[i].length;c++) {
                                    for (let j = 0; j < self.result.length; j++) {
                                        if (self.result[j]['serverId'] == rNstatex[i][c]['parentinfo']['sharq']) {
                                            self.result[j]['nstatexGroup'].push(rNstatex[i][c])
                                        }
                                    }
                                }
                            }
                            // console.log(".............")
                            resolve(self.result)
                        })
                        .catch(function (error) {
                            console.log(".............",error)
                            resolve({error:true,errMsg:error.toString()})
                        })

                    // resolve(self.result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.selectSharq = function () {
        console.log("self.segmentId",self.segmentId)
        return new Promise(function (resolve, reject) {
            qartez.findByMultyNamePromise("map_sharq",{segment_id:self.segmentId},['id','segment_id','group_properties','properties','left_namelable_properties','left_text_properties','right_namelable_properties','right_text_properties'])
                .then(function (data) {
                    if(self.segmentId == '31'){
                        // console.log("data",data)
                    }
                    for(let i=0;i<data.length;i++){
                        let group_properties = JSON.parse(data[i]['group_properties']);
                        group_properties['id'] = "group_sharq_"+data[i]['id'];
                        group_properties['name'] = "group_sharq_"+data[i]['id'];
                        let children = JSON.parse(data[i]['properties']);
                        children['id'] = data[i]['id'];
                        children['name'] = data[i]['id'];
                        children['parentinfo']['segment'] = data[i]['segment_id'];
                        self.result.push({
                            segment_id:data[i]['segment_id'],
                            serverId:data[i]['id'],
                            id:"group_sharq_"+data[i]['id'],
                            name:"group_sharq_"+data[i]['id'],
                            properties:group_properties,
                            children:children,
                            left_namelable_pro:JSON.parse(data[i]['left_namelable_properties']),
                            left_text_pro:JSON.parse(data[i]['left_text_properties']),
                            right_namelable_pro:JSON.parse(data[i]['right_namelable_properties']),
                            right_text_pro:JSON.parse(data[i]['right_text_properties']),
                            nstatexGroup:[]
                        })
                    }
                    resolve()
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.insertSharq = function () {
        return new Promise(function (resolve, reject) {
            let child = self.children;
            delete child.thisclass;
            query.insert2vPromise("map_sharq",{
                segment_id:self.segmentId,
                group_properties:JSON.stringify(self.properties),
                properties:JSON.stringify(child),
                left_namelable_properties:JSON.stringify(self.left_namelable_pro),
                left_text_properties:JSON.stringify(self.left_text_pro),
                right_namelable_properties:JSON.stringify(self.right_namelable_pro),
                right_text_properties:JSON.stringify(self.right_text_pro)
            })
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }
}


module.exports = Sharq;
