var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var sha1 = require('sha1');
var staticMethods = require('../../model/staticMethods');

const pagePermissions = '1';

router.get('/', function(req, res, next) {
    // let renderPage = 'Profile/settings';
    // query.getPlaceInfo(req.userInfo['id'], function (data) {
    //     var pageInfo = data;
        // console.log('pageInfo',pageInfo);
        res.render('Profile/settings',
            {
                userInfo:req.userInfo,
                language : req.language,
                siteData:req.siteData,
                // selfPermissions : checkPermission.readOrWrite,
                // data:pageInfo
            });

    // })
});

router.post('/',function (req, res, next) {
    let selfData = JSON.parse(req.body.data);

    if(
        typeof req.body.place_id != 'undefined' &&
        typeof req.body.phone != 'undefined' &&
        req.body.phone.length > 7 &&
        typeof req.body.email != 'undefined' &&
        req.body.email.length != 0
    ){
        var updateData =
            {
                values: {
                    phone:req.body.phone,
                    email:req.body.email
                },
                where: {
                    id:req.body.place_id
                }
            };
        // console.log('updateData',updateData)
        query.update2vPromise('places',updateData)
            .then(function (data) {
                var querysArr = [];
                for(var i = 0; i < selfData.length; i++){
                    var updateInfo =
                        {
                            values: {
                                name:selfData[i].name,
                                address:selfData[i].address
                            },
                            where: {
                                lang_id:selfData[i].lang_id,
                                id:selfData[i].place_lang_id
                            }
                        };
                    querysArr.push(query.update2vPromise('places_lang',updateInfo));
                }
                Promise.all(querysArr)
                    .then(function () {
                        res.redirect('/profile/settings');
                    })
                    .catch(function (error) {
                        res.json({error:true,errorMsg:error.toString(),msg:'1534864168163651'});
                        res.end();
                    })
            })
            .catch(function (error) {
                res.json({error:true,errorMsg:error.toString(),msg:'87941323068435'});
                res.end();
            })
    }
});


// router.post('/update-password',function (req, res, next) {
//     staticMethods.checkPermissions(pagePermissions,req.userInfo.permissions)
//         .then(function (checkPermission) {
//             if(checkPermission.readOrWrite == '1'){
//                 let oldPass = sha1(req.body.oldPass);
//                 let newPass = req.body.newPass;
//                 let newRePass = req.body.newRePass;
//                 query.checkUserByPasswordAndToken(oldPass,req.userInfo.token)
//                     .then(function (data) {
//                         if(data.length === 1){
//                             if(newPass === newRePass){
//                                 var updateData = {values:{pass:sha1(newPass)},where: {id:data[0]['id']}};
//                                 query.update2v('admin_clients',updateData, function (error) {
//                                     if(!error){
//                                         res.clearCookie("clin");
//                                         res.json({error:false,msg:'1000'})
//                                     }else{
//                                         res.json({error:true,msg:'4861584316156'})
//                                     }
//                                 })
//                             }else{
//                                 res.json({error:true,msg:'4748615335135'})
//                             }
//                         }else{
//                             res.json({error:true,msg:'13183151315'})
//                         }
//                     })
//                     .catch(function (error) {
//                         res.json({error:true,errorMsg:error.toString(),msg:'5268512685316'});
//                         res.end();
//                     })
//             }else{
//                 res.json({error:'/permission-denied',msg:'486513205683521'});
//                 res.end();
//             }
//         })
// });


// function checkPermissions(pagePermissions,permissions){
//     let selfPermissions = [];
//     for(let i = 0; i < permissions.length; i++){
//         if(permissions[i]['id'] == pagePermissions){
//             selfPermissions.push(permissions[i]);
//             break;
//         }
//     }
//     let access = true;
//     if(selfPermissions[0].is_open == '0' ){
//         access = false;
//     }
//     let retObj = {
//         access : access,
//         readOrWrite : selfPermissions[0].read_or_write
//     };
//     return retObj
// }

module.exports = router;
