var Sharq = require('./Sharq');
var query = require('../../model/dbQuerys/querys');
var qartez = require('../../model/dbQuerys/qartez');

function Map(mapId,data,nstatexGroupPrice) {

    this.mapId = mapId;
    this.id = typeof data != "undefined" ? data.id : null;
    this.name = typeof data != "undefined" ? data.name : null;
    this.properties = typeof data != "undefined" ? data.properties : null;
    this.children = typeof data != "undefined" ? data.children : null;
    this.nameLable_properties = typeof data != "undefined" ? data.nameLable_properties : null;
    this.text_properties = typeof data != "undefined" ? data.text_properties : null;
    this.sharq = typeof data != "undefined" ? data.sharq : null;
    this.nstatexGroupPrice = nstatexGroupPrice != "undefined" ?  nstatexGroupPrice : null;


    this.segment_id = null;

    this.result = {
        name:"",
        version:"",
        info:[]
    }

    var self = this;

    this.init = function () {
        return new Promise(function (resolve, reject) {
            self.insertSegment()
                .then(function (data) {
                    self.segment_id = data.insertId;
                    let allSharq = [];
                    for(let i=0;i<self.sharq.length;i++){
                        let sharqs = new Sharq(self.segment_id,self.sharq[i],self.nstatexGroupPrice);
                        allSharq.push(sharqs.init())
                    }

                    Promise.all(allSharq)
                        .then(function () {
                            resolve(data)
                        })
                        .catch(function (error) {
                            resolve({error:true,errMsg:error.toString()})
                        })

                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.initSelect = function (mapid) {
        this.mapId = mapid;
        return new Promise(function (resolve, reject) {
            self.selectSegment()
                .then(function (data) {

                    let allSharq = [];
                    for(let i=0;i<self.result.info.length;i++){
                        let sharqs = new Sharq(self.result.info[i]['serverId']);
                        allSharq.push(sharqs.initSelect(self.result.info[i]['serverId'],self.nstatexGroupPrice))
                    }

                    Promise.all(allSharq)
                        .then(function (resultSharq) {
                            console.log("................",resultSharq.length)
                            // console.log("resultSharq",resultSharq[0][0]['segment_id'])
                            // console.log("resultSharq",resultSharq[0][1]['segment_id'])
                            let rSharq = resultSharq;
                            for(let i=0;i<rSharq.length;i++){
                                for(let k=0;k<rSharq[i].length;k++) {
                                    for (let j = 0; j < self.result.info.length; j++) {
                                        if (self.result.info[j]['serverId'] == rSharq[i][k]['segment_id']) {
                                            self.result.info[j]['sharq'].push(rSharq[i][k])
                                        }
                                    }
                                }
                            }
                            console.log("resultSharq",self.result)
                            resolve(self.result)
                        })
                        .catch(function (error) {
                            resolve({error:true,errMsg:error.toString()})
                        })

                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.selectSegment = function () {
        return new Promise(function (resolve, reject) {
            qartez.findByMultyNamePromise("map_segment",{map_id:self.mapId},['id','group_properties','properties','namelable_properties','text_properties'])
                .then(function (data) {
                    for(let i=0;i<data.length;i++){
                        let group_properties = JSON.parse(data[i]['group_properties']);
                        group_properties['id'] = "group_"+data[i]['id'];
                        group_properties['name'] = "group_"+data[i]['id'];
                        let children = JSON.parse(data[i]['properties']);
                        children['id'] = data[i]['id'];
                        children['name'] = data[i]['id'];
                        self.result.info.push({
                            serverId:data[i]['id'],
                            id:"group_"+data[i]['id'],
                            name:"group_"+data[i]['id'],
                            properties:group_properties,
                            children:children,
                            nameLable_properties:JSON.parse(data[i]['namelable_properties']),
                            text_properties:JSON.parse(data[i]['text_properties']),
                            sharq:[]
                        })
                    }
                    resolve()
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }

    this.insertSegment = function () {
        return new Promise(function (resolve, reject) {
            let child = self.children;
            delete child.thisclass;
            query.insert2vPromise("map_segment",{
                map_id: self.mapId,
                group_properties:JSON.stringify(self.properties),
                properties:JSON.stringify(child),
                namelable_properties:JSON.stringify(self.nameLable_properties),
                text_properties:JSON.stringify(self.text_properties)
            })
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }
}


module.exports = Map;
