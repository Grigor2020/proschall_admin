var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
const schedule = require('node-schedule');
var statics = require('../../static');
var request = require('request');
var moment = require('moment');
var staticMethods = require('../../model/staticMethods');

router.get('/', function(req, res, next) {
    console.log('getDate',new Date().getDate())
    console.log('getHours',new Date().getHours())
    console.log('getMinutes',new Date().getMinutes())
    query.findAllTournaments()
        .then(function (data) {
            res.render('Tournament/tournament',
                {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    language : req.language,
                    data:data
                });
            // res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});

router.get('/add', function(req, res, next) {
    Promise.all([
        query.findAllConsoles(null),
        query.findAllGames(null)
    ])

        .then(function ([consoles,games]) {
            res.render('Tournament/tournamentItem',
                {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    language : req.language,
                    games : games,
                    consoles : consoles
                });
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })

});

router.post('/get-game-modes', function(req, res, next) {
    let gameId = req.body.game_id;
    query.getGameModes(gameId)
        .then(function (modes) {
            res.json({error : false,modes : modes});
            res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })

});

router.post('/add', function(req, res, next) {
    var convertedStartDatetime = new Date(req.body.start_datetime).toUTCString();
    var convertedCheckInStartDatteime = new Date(req.body.check_in_datetime).toUTCString()
    var tournamentPercent = 10;
    // let members_count = parseFloat(req.body.members_count);
    let entry = parseFloat(req.body.entry);
    var insertData = {
        name : req.body.name,
        game_id : req.body.game_id,
        consoles_id : req.body.consoles_id,
        game_mode_id : req.body.game_mode_id,
        members_count : 8,
        prize_1 : req.body.prize_1,
        prize_2 : req.body.prize_2,
        entry : req.body.entry,
        start_datetime : req.body.start_datetime,
        check_in_datetime : req.body.check_in_datetime,
    };
    console.log('check_in_datetime',insertData.check_in_datetime)

    var checkInDate = new Date(req.body.check_in_datetime);
    let datesForEmail = new Date(checkInDate.getTime() - 10*60000)
    console.log('datesForEmail',datesForEmail)
    // let myCheckInTime = staticMethods.getTimeOptions(req.body.check_in_datetime)
    // let myStartTime = staticMethods.getTimeOptions(req.body.start_datetime)
    //
    // let checkInDate = new Date(''+myCheckInTime.year+'-'+myCheckInTime.month+'-'+myCheckInTime.day+'T'+myCheckInTime.hour+':'+myCheckInTime.minute+':00z');
    // let startDate = new Date(''+myStartTime.year+'-'+myStartTime.month+'-'+myStartTime.day+'T'+myStartTime.hour+':'+myStartTime.minute+':00z');


    let fullYear = checkInDate.getFullYear();
    let month = checkInDate.getMonth();
    let day = checkInDate.getDate();
    let time = checkInDate.getHours();
    let minute = checkInDate.getMinutes();
    let second = checkInDate.getSeconds();
    let datesForEmailfullYear = datesForEmail.getFullYear();
    let datesForEmailmonth = datesForEmail.getMonth();
    let datesForEmailday = datesForEmail.getDate();
    let datesForEmailtime = datesForEmail.getHours();
    let datesForEmailminute = datesForEmail.getMinutes();
    let datesForEmailsecond = datesForEmail.getSeconds();

    // console.log('datesForEmailfullYear',datesForEmailfullYear)
    // console.log('datesForEmailmonth',datesForEmailmonth+1)
    // console.log('datesForEmailday',datesForEmailday)
    // console.log('datesForEmailtime',datesForEmailtime)
    // console.log('datesForEmailminute',datesForEmailminute)
    // console.log('datesForEmailsecond',datesForEmailsecond)


    var date = new Date(fullYear, month, day, time, minute, second);
    var datesForEmaildate = new Date(datesForEmailfullYear, datesForEmailmonth, datesForEmailday, datesForEmailtime, datesForEmailminute, datesForEmailsecond);
    // console.log('date',date)
    console.log('datesForEmaildate',datesForEmaildate)
    // datesForEmaildate.setHours( datesForEmaildate.getHours() + 4 );
    // console.log('+4 datesForEmaildate',datesForEmaildate)
    // // insertData.prize = (8 * entry) - ((8 * entry) * tournamentPercent / 100);
    query.insert2vPromise('tournament',insertData)
        .then(function (insertResult) {
            console.log('insertResult',insertResult)
            var j = schedule.scheduleJob(''+insertResult.insertId,datesForEmaildate, function(){
                console.log('JOBBB j')
                const options = {
                    url: statics.API_URL + '/tournament/send-tournament-stats-to-admin',
                    method: "POST",
                    headers: {
                        authorization: statics.API_AUTH
                    },
                    form: {
                        "tournament_id": insertResult.insertId
                    }
                };
                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        const result = JSON.parse(body);
                        console.log('result', result)
                        if (!result.error) {

                        }
                    }
                    j.cancel()
                });

            });
            var k = schedule.scheduleJob(''+insertResult.insertId,date, function(){
                console.log('JOBBB k')
                const options = {
                    url: statics.API_URL + '/tournament/send-users-checkin-emails',
                    method: "POST",
                    headers: {
                        authorization: statics.API_AUTH
                    },
                    form: {
                        "tournament_id": insertResult.insertId
                    }
                };
                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        const result = JSON.parse(body);
                        console.log('result', result)
                        if (!result.error) {

                        }
                    }
                    k.cancel()
                });

            });
            console.log('fff - j',j)
            console.log('fff -  k',k)
            // res.redirect('/tournament')
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});
module.exports = router;
