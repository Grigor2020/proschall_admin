const MAIN_URL = 'http://prizchall.com';
const API_URL = 'http://api.prizchall.com/v1';
// const MAIN_URL = 'http://127.0.0.1:3002';
// const API_URL = 'http://127.0.0.1:3003/v1';
const API_AUTH = "5b99975c1a84dc272e7beb8ac35f02e7a861615c";
const SITE_PERCENT = 10;

module.exports.MAIN_URL = MAIN_URL;
module.exports.API_URL = API_URL;
module.exports.API_AUTH = API_AUTH;
module.exports.SITE_PERCENT = SITE_PERCENT;


// var statics = require('../localStatic');
//
// module.exports.MAIN_URL = statics.MAIN_URL;
// module.exports.API_URL = statics.API_URL;
// module.exports.API_AUTH = statics.API_AUTH;
// module.exports.SITE_PERCENT = statics.SITE_PERCENT;
