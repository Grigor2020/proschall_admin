var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var staticMethods = require('../../model/staticMethods');
var multer = require('multer');
var upload = multer();
var log = require("../../logs/log");

upload.storage = multer.diskStorage({
    // destination: './public/images/games/',
    destination: '../api/public/images/games/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = Date.now() + '-' + random+"."+type[1];
        cb(null, fullName);
    }
});

router.get('/update/:id', function(req, res, next) {
    console.log('req.params.id',req.params.id)
});
router.get('/', function(req, res, next) {
    query.findAllGames(null)
        .then(function (data) {
            res.render('games/games', {
                userInfo:req.userInfo,
                siteData:req.siteData,
                language:req.language,
                games:data
            });
        })
        .catch(function (error) {
            log.insertLog("get all games : ","./logs/all-logs.txt");
            res.json({error:true,msg :'984351631535313513',errMsg : error.toString()});
            res.end();
        })

});

router.get('/add', function(req, res, next) {
    console.log('language',req.language)
    query.findAllPromise('consoles',null)
        .then(function (data) {
            res.render('games/addGames', {
                userInfo:req.userInfo,
                siteData:req.siteData,
                language:req.language,
                consoles : data
            });
        })
        .catch(function (error) {
            log.insertLog("get all consoles : ","./logs/all-logs.txt");
            res.json({error:true,msg :'98435167455548613',errMsg : error.toString()});
            res.end();
        })
});
router.get('/mode', function(req, res, next) {
    query.findAllModes(null)
        .then(function (data) {
            console.log('data',data)
            res.render('games/gameMode', {
                userInfo:req.userInfo,
                siteData:req.siteData,
                language:req.language,
                modes:data
            });
        })
        .catch(function (error) {
            log.insertLog("get all modes : ","./logs/all-logs.txt");
            res.json({error:true,msg :'98435167455548613',errMsg : error.toString()});
            res.end();
        })
});
router.get('/server', function(req, res, next) {
    query.findAllServers(null)
        .then(function (data) {
            res.render('games/gameServer', {
                userInfo:req.userInfo,
                siteData:req.siteData,
                language:req.language,
                servers:data
            });
        })
        .catch(function (error) {
            log.insertLog("get all Server : ","./logs/all-logs.txt");
            res.json({error:true,msg :'984351654333548613',errMsg : error.toString()});
            res.end();
        })
});
router.get('/server/add', function(req, res, next) {
    query.findAllGames(null)
        .then(function (data) {
            res.render('games/addGameServer', {
                userInfo:req.userInfo,
                language:req.language,
                siteData:req.siteData,
                games:data
            });
        })
        .catch(function (error) {
            log.insertLog("get all games : ","./logs/all-logs.txt");
            res.json({error:true,msg :'9843516315158613',errMsg : error.toString()});
            res.end();
        })
});
router.get('/mode/add', function(req, res, next) {
    query.findAllGames(null)
        .then(function (data) {
            res.render('games/addGameMode', {
                userInfo:req.userInfo,
                siteData:req.siteData,
                language:req.language,
                games:data
            });
        })
        .catch(function (error) {
            log.insertLog("get all games : ","./logs/all-logs.txt");
            res.json({error:true,msg :'9843516315158613',errMsg : error.toString()});
            res.end();
        })
});


router.post('/mode/add',function (req, res, next) {
    var game_modeInsertInfo = {
        game_id : req.body.game_id,
        user_id : null,
        from_admin : '1',
        name : req.body.name,
    };
    query.insert2vPromise('game_mode',game_modeInsertInfo)
        .then(function (data) {
            res.redirect('/games/mode')
        })
        .catch(function (error) {
            log.insertLog("game mode insert error, URL = /games/mode/add : ","error-"+ error.toString()+" , ./logs/all-logs.txt");
            res.json({error:true,msg :'15316841335213464',errMsg : error.toString()});
            res.end();
        })
});


router.post('/server/add',function (req, res, next) {
    var game_modeInsertInfo = {
        game_id : req.body.game_id,
        name : req.body.name,
    };
    query.insert2vPromise('game_server',game_modeInsertInfo)
        .then(function (data) {
            res.redirect('/games/server')
        })
        .catch(function (error) {
            log.insertLog("game server insert error, URL = /games/server/add : ","error-"+ error.toString()+" , ./logs/all-logs.txt");
            res.json({error:true,msg :'1535534564534',errMsg : error.toString()});
            res.end();
        })
});

var cpUpload = upload.fields([
    { name: 'image', maxCount: 1 }
]);
router.post('/add',cpUpload,function (req, res, next) {
    let res_data = JSON.parse(req.body.res_data);
    var url = "";
    if(req.files['image'][0].filename.length > 0){
        for(let i = 0; i < res_data.length; i++){
            if(res_data[i].lang_id == '1'){
                url = staticMethods.generateUrlFrmoString(res_data[i].name)
            }
        }

        var gamesInsertInfo = {
            image : req.files['image'][0].filename,
            url : url
        };
        query.insert2vPromise('games',gamesInsertInfo)
            .then(function (games_insert_result) {
                var gamesId = games_insert_result.insertId;
                let langs_filds = ['lang_id','games_id','name'];
                let langs_loopPost = [];
                for(let i = 0; i < res_data.length; i++){
                    langs_loopPost.push([''+res_data[i].lang_id,''+gamesId,''+res_data[i].name]);
                }
                console.log(langs_loopPost)
                Promise.all([
                    query.insertLoopPromise('games_language',langs_filds,langs_loopPost),
                ])
                    .then(function ([games_lang_insert_result]) {
                        res.redirect('/games')
                    })
                    .catch(function (error) {
                        log.insertLog("games_lang insert error, URL = /games/add : ","error-"+ error.toString()+" , ./logs/all-logs.txt");
                        res.json({error:true,msg :'1531684133514841863',errMsg : error.toString()});
                        res.end();
                    })

            })
            .catch(function (error) {
                log.insertLog("games insert error, URL = /games/add : ","./logs/all-logs.txt");
                res.json({error:true,msg :'45365832316815',errMsg : error.toString()});
                res.end();
            })
    }else{
        log.insertLog("games image not uploaded  error, URL = /games/add : ","./logs/all-logs.txt");
        res.json({error:true,msg :'12368431564315',errMsg : null});
        res.end();
    }

});

module.exports = router;
