class Nstatexv2 {

    constructor(number,id,x,y,w,h,imageObj,draggable,parentsInfo,groupPrice){
        this.number = number;
        this.id = id;
        this.x = x;
        this.y = y;
        this.image = imageObj;
        this.width = w;
        this.height = h;
        this.draggable = draggable || false;
        this.fill = null,//"#00f821";
        this.stroke = null,//"#00f821";
        this.strokeWidth = 0;
        this.cornerRadius = 0;
        this.shadowBlur = 0;
        this.shadowColor = "#FFFFFF";
        this.isSelect = false;

        this.parentinfo = {
            segment:parentsInfo.segment,
            sharq:parentsInfo.sharq,
            segment_name:parentsInfo.segment_name,
            sharq_name:parentsInfo.sharq_name
        };

        this.group = groupPrice == null ? null : groupPrice;

        this.imageTooltip = null;
        this.chairNumber = null;
        this.rect = null;
        this.klor = null;

        // this.serverInfo = serverInfo || null;
    }

    init(){
        this.rect = new Konva.Rect({
            typeobj:"nstatex",
            number:this.number,
            id:"id_"+this.id,
            name:"nstatex_"+this.id,
            isTransform:false,
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            fill: this.fill,
            stroke: this.stroke,
            strokeWidth: this.strokeWidth,
            cornerRadius: this.cornerRadius,
            parentinfo: this.parentinfo,
            group: this.group,
            // shadowBlur: this.shadowBlur,
            // shadowColor: this.shadowColor,
            // shadowOffset: { x: 0, y: 0 },
            draggable: this.draggable,
            thisclass:this,
            isSelect:this.isSelect
        })

        this.generateToltip();
        this.generateCircle();
    }

    generateCircle(){
        this.klor = new MyCircle(this.id,this.x + this.width/2,this.y + this.height/2,7.5, "#51FF00", null,null,this)
        this.klor.init();
        // console.log(this.klor.getObject())
    }

    generateToltip(){
        var self = this;

        this.imageTooltip = new Konva.Label({
            x: self.x + self.width/2,
            y: self.y - 5,
            opacity: 1,
            visible: false
        });

        this.imageTooltip.add(
            new Konva.Tag({
                fill: '#FFFFFF',
                pointerDirection: 'down',
                pointerWidth: 10,
                pointerHeight: 10,
                lineJoin: 'round',
                cornerRadius: 2,
                shadowColor: null,
                shadowBlur: 10,
                shadowOffsetX: 10,
                shadowOffsetY: 10,
                shadowOpacity: 0.5
            })
        );

        let textShow = " "+this.parentinfo.segment_name+"\n Շարք: "+this.parentinfo.sharq_name+"\n Տեղ: "+this.number;
        this.imageTooltip.add(
            new Konva.Text({
                text: textShow,
                fontFamily: 'sans-serif',
                fontSize: 14,
                padding: 5,
                fill: '#161616'
            })
        );

        this.chairNumber = new Konva.Label({
            x: this.number < 10 ? self.x + self.width/2 - 3 : self.x + self.width/2 - 6.5,
            y: self.y + self.height/2 - 5,
            opacity: 1,
            visible: false
        });

        this.chairNumber.add(
            new Konva.Text({
                text: this.number,
                fontFamily: 'sans-serif',
                fontSize: this.number < 10 ? 12 : 12,
                fill: '#161616',
            })
        )
    }

    updateText(){
        let textShow = this.parentinfo.segment_name+"\n Շարք: "+this.parentinfo.sharq_name+"\n Տեղ: "+this.number;
        this.imageTooltip['children'][1].setAttrs({
            text: textShow
        });
    }

    initEvents(){
        var self = this;
        // this.rect.on('transformstart', function() {
        //     // console.log('transform start');
        // });
        //
        // this.rect.on('dragmove', function() {
        //     //this.layer.batchDraw();
        // });
        // this.rect.on('transform', function() {
        //     this.setAttrs({
        //         width: this.width() * this.scaleX(),
        //         height: this.height() * this.scaleY(),
        //         scaleX: 1,
        //         scaleY: 1
        //     });
        //     // console.log('transform');
        // });
        //
        // this.rect.on('transformend', function() {
        //     // console.log('transform end');
        // });

        this.rect.on('mousemove', function() {
            self.rect.setAttrs({
                // shadowBlur: 5,
                // fill: "#FFFFFF",
                // strokeWidth:2
            });
            self.imageTooltip.show();
            self.chairNumber.show();
        });

        this.rect.on('mouseout', function() {
            // if(!self.rect.attrs.isSelect) {
            //     console.log("mouseout",self.rect.attrs.isSelect)
            //
            // }
            if(!self.rect.attrs.isSelect) {
                self.rect.setAttrs({
                    stroke: "#00f821",
                    strokeWidth: 0
                });
            }
            self.imageTooltip.hide();
            self.chairNumber.hide();
        });

        this.rect.on('mousedown', function() {
            // if(!self.rect.attrs.isSelect) {
            //     removeNstatexSelect();
            // }
            removeNstatexSelect();
            self.rect.setAttrs({
                stroke: "#00f821",
                strokeWidth: 1
            });
            getSelectObjInfo(self.rect)
            self.imageTooltip.show();
            self.chairNumber.show();
        });

        this.rect.on('dblclick', function() {
            self.getCoordinates()
        });

        this.parent.on('dragmove', function() {
            getSelectObjInfo(self.rect)
        });
    }

    setGroup(){
        this.rect.setAttrs({
            fill: this.group.color
        });
    }

    onMouseDown(){
        getSelectObjInfo(this.rect);
        triggerMouseEvent (this.rect, "mousedown");
    }

    onMouseMove(){
        this.imageTooltip.show();
        this.chairNumber.show();
    }

    onMouseOut(){
        if(!this.rect.attrs.isSelect) {
            this.rect.setAttrs({
                stroke: "#00f821",
                strokeWidth: 0
            });
        }
        this.imageTooltip.hide();
        this.chairNumber.hide();
    }

    setParent(parents){
        this.parent = parents;
        this.initEvents();
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.rect.x(),
            'y: ' + this.rect.y(),
            'rotation: ' + this.rect.rotation(),
            'width: ' + this.rect.width(),
            'height: ' + this.rect.height(),
            'scaleX: ' + this.rect.scaleX(),
            'scaleY: ' + this.rect.scaleY()
        ];
        // console.log(lines.join('\n'));
    }

    getObject(){
        return this.rect;
    }

    getTootipObject(){
        return this.imageTooltip;
    }

    getNumberObject(){
        return this.chairNumber;
    }

    getKlor(){
        return this.klor.getObject();
    }

}
