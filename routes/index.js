var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');



/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language,
        title: 'Proshcall Admin'
    });

});

module.exports = router;
