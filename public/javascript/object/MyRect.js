class MyRect {

    constructor(id,x,y,w,h,fill,stroke,stokeW,draggable,topbottom,nameText,serverInfo){
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.fill = fill || null;
        this.stroke = stroke || null;
        this.strokeWidth = stokeW || null;
        this.draggable = draggable || false;

        this.showName = true;
        this.topbottom = topbottom || "top";

        this.isRender = false;
        this.parent = null;
        this.rect = null;
        this.nameLable = null;

        this.serverId = serverInfo != null ? serverInfo.id : null;
        this.showNameText = nameText || id;
        this.serverInfo = serverInfo || null;
    }

    init(){
        // console.log("parent",parents)
        // this.parent = parents;
        this.rect = new Konva.Rect({
            typeobj:"segment",
            id:this.id,
            name:this.id,
            isTransform:true,
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            fill: this.fill,
            stroke: this.stroke,
            strokeWidth: this.strokeWidth,
            draggable: this.draggable,
            showName: this.showName,
            topbottom: this.topbottom,
            showNameText: this.showNameText,
            serverId: this.serverId,
            thisclass:this
        })

        if(this.serverInfo == null) {
            this.generateLable();
        }else{
            this.generateServerLable();
        }
    }

    generateLable(){
        var self = this;

        this.nameLable = new Konva.Label({
            x: 0,
            y: 0,
            opacity: 1,
            visible: this.showName
        });

        this.nameLable.add(
            new Konva.Text({
                text: self.showNameText,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
        if(this.topbottom == "top") {
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
            this.nameLable.y(-30);
        }else if(this.topbottom == "bottom"){
            this.nameLable.y(self.height + 20);
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
        }else{
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
            this.nameLable.y(-30);
        }
    }

    generateServerLable(){
        var self = this;

        this.nameLable = new Konva.Label({
            x: this.serverInfo.nameLable.x,
            y: this.serverInfo.nameLable.y,
            opacity: 1,
            visible: this.showName
        });

        this.nameLable.add(
            new Konva.Text({
                text: this.serverInfo.text.text,
                fontFamily: 'sans-serif',
                fontSize: 18,
                fill: '#FFFFFF',
            })
        )
        if(this.topbottom == "top") {
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
            this.nameLable.y(-30);
        }else if(this.topbottom == "bottom"){
            this.nameLable.y(self.height + 20);
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
        }else{
            this.nameLable.x(self.x - this.nameLable.width() / 2 + self.width / 2);
            this.nameLable.y(-30);
        }
    }

    initEvents(){
        var self = this;
        this.rect.on('transformstart', function() {
            // console.log('transform start');
        });

        this.rect.on('dragmove', function() {
            //this.layer.batchDraw();
        });
        this.rect.on('transform', function() {
            this.setAttrs({
                width: this.width() * this.scaleX(),
                height: this.height() * this.scaleY(),
                scaleX: 1,
                scaleY: 1
            });
            getSelectObjInfo(self.rect)
            // console.log('transform');
        });

        this.rect.on('transformend', function() {
            let w = Math.round(this.width()/grid)*grid;
            let h = Math.round(this.height()/grid)*grid;
            this.setAttrs({
                width: w,
                height: h
            });

            if(self.topbottom == "top") {
                self.nameLable.x(this.x() - self.nameLable.width() / 2 + this.width() / 2);
                self.nameLable.y(-30);
            }else if(self.topbottom == "bottom"){
                self.nameLable.y(this.height() + 20);
                self.nameLable.x(this.x() - self.nameLable.width() / 2 + this.width() / 2);
            }else{
                self.nameLable.x(this.x() - self.nameLable.width() / 2 + this.width() / 2);
                self.nameLable.y(-30);
            }
            getSelectObjInfo(self.rect)
        });

        this.rect.on('mousedown', function() {
            removeNstatexSelect();
            removeSelectedNstatexSelect();
        });

        this.parent.on('dragmove', function() {
            getSelectObjInfo(self.rect)
        });

        this.rect.on('dblclick', function() {
            self.getCoordinates()
        });
    }

    setParent(parents){
        this.parent = parents;
        this.initEvents();
    }

    onMouseDown() {
        removeNstatexSelect();
        removeSelectedNstatexSelect();
        getSelectObjInfo(this.rect);
        triggerMouseEvent (this.rect, "mousedown");
    }

    updateText(){
        let self = this;
        this.nameLable['children'][0].setAttrs({
            text: self.showNameText
        });
        if(this.topbottom == "top") {
            this.nameLable.x(this.rect.x() - this.nameLable.width() / 2 + this.rect.width() / 2);
            this.nameLable.y(-30);
        }
    }

    showHideName(){
        if(this.showName){
            this.nameLable.show();
        }else{
            this.nameLable.hide();
        }
    }

    topOrBottomName(){
        if(this.topbottom == "top"){
            this.nameLable.x(this.rect.x() - this.nameLable.width() / 2 + this.rect.width() / 2);
            this.nameLable.y(-30);
        }else if(this.topbottom  == "bottom"){
            this.nameLable.y(this.rect.height() + 20);
            this.nameLable.x(this.rect.x() - this.nameLable.width() / 2 + this.rect.width() / 2);
        }else{
            this.nameLable.x(this.rect.x() - this.nameLable.width() / 2 + this.rect.width() / 2);
            this.nameLable.y(-30);
        }
    }

    getCoordinates(){
        var lines = [
            'x: ' + this.rect.x(),
            'y: ' + this.rect.y(),
            'rotation: ' + this.rect.rotation(),
            'width: ' + this.rect.width(),
            'height: ' + this.rect.height(),
            'scaleX: ' + this.rect.scaleX(),
            'scaleY: ' + this.rect.scaleY()
        ];
        console.log(lines.join('\n'));
    }

    getObject(){
        return this.rect;
    }

    getLableObject(){
        return this.nameLable;
    }
}
