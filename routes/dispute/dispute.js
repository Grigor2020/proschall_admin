var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var sha1 = require('sha1');
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static');


router.get('/', function(req, res, next) {
    query.findAllDispute()
        .then(function (data) {
            res.render('Dispute/dispute',
                {
                    userInfo:req.userInfo,
                    siteData:req.siteData,
                    language : req.language,
                    data:data
                });
            // res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});

router.get('/details/:id', function(req, res, next) {
    var disputeId = req.params.id;
    query.findDisputeById(disputeId)
        .then(function (data) {
            Promise.all([
                query.findByMultyNamePromise('users_db',{id:data[0].creator_id},['firstName','lastName','username','email','e_verify']),
                query.findByMultyNamePromise('users_db',{id:data[0].joiner_id},['firstName','lastName','username','email','e_verify'])
            ])
                .then(function ([creatorInfo,joinerInfo]){
                    console.log('creatorInfo',creatorInfo)
                    console.log('joinerInfo',joinerInfo)
                    data[0].file = statics.API_URL+'/images/dispute/'+data[0].filename;
                    res.render('Dispute/dispute_item',
                        {
                            userInfo:req.userInfo,
                            siteData:req.siteData,
                            language : req.language,
                            data:data[0],
                            users:{
                                creatorInfo:creatorInfo[0],
                                joinerInfo:joinerInfo[0]
                            }
                        });
                })

            // res.end();
        })
        .catch(function (error) {
            res.json({error:true});
            res.end();
        })
});

module.exports = router;
